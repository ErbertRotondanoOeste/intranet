![alt text](https://yata-apix-61589086-5007-4240-9eec-744605e3805f.lss.locawebcorp.com.br/7d4eff6626864e6ab5be2ae448c898df.png "Logo Oeste Tecnologia")

## Projeto de Intranet da Oeste Tecnologia

Esse projeto é um trabalho interno da equipe de desenvolvimento da Oeste Tecnologia, antes de continuar certifique-se que você possui autorização para estar nessa página.

## Para implementar o projeto no seu computador

- [Instale o laragon](https://laragon.org/);
- No seu terminal clone uma instância do projeto usando o comando git clone no topo da página.
- No terminal instale as dependências do composer usando: `composer install`.
- No terminal instale as dependências do npm usando: `npm install` ou `yarn install`.
- Crie um arquivo .env com as variáveis de configuração de ambiente usando: `cp .env.example .env`.
- Configure o arquivo .env gerado com os dados locais da sua configuração de banco de dados.
- Gere uma chave de segurança da sua instalação local usando: `php artisan key:generate`.
- Crie uma branch substituindo NOME_DA_BRANCH pelo nome de sua preferência usando: `git checkout -b NOME_DA_BRANCH`. Use letras minúsculas
- Inicie as estruturas do banco de dados usando: `php artisan migrate --seed`.
- Teste e comece a codar...

## Padrões de commit:

- feat(<contexto>):<descricao>  :  O feat serve para adicionar novas coisas ao projeto
- refactor(<contexto>):<descricao>:  O refactor serve para editar coisas existentes, refatorar bugs, etc.
- fix(<contexto>):<descricao>  :  O fix serve para consertar bugs que deixam o sistema não funcional (quebrado)



