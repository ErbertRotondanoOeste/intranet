<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/user/{user_id?}', 'UserController@profile')->name('profile');

Route::get('/my_paychecks/months', 'PaycheckController@myPaycheckMonths')->name('myPaycheckMonths');
Route::get('/my_paychecks/months/{user_id?}/{month_id?}', 'PaycheckController@paycheckList')->name('myPaychecks');

Route::get('/', 'HomeController@index')->name('home');
Route::post('/login', 'Auth\LoginController@authenticate')->name('login');
Route::get('/permission-denied', 'PermissionController@permissionDenied')->name('nopermission');

Route::get('/updatepassword', 'UserController@updatePassword')->name('updatePassword');
Route::post('/updatepassword', 'UserController@updatePassword')->name('updatePassword');

Route::get('user/{user_id?}/months/', 'PaycheckController@userMonths')->name('userMonths');
Route::get('user/{user_id?}/month/{month_id?}', 'PaycheckController@userPaychecks')->name('userPaychecks');
Route::get('paycheck/{paycheck_id?}/sign', 'PaycheckController@signPaycheck')->name('signPaycheck');
Route::post('downloadfile', 'FileController@downloadFile')->name('downloadFile');
Route::get('purchases/create', 'PurchaseController@create')->name('createPurchase');
Route::post('purchases/create', 'PurchaseController@create')->name('createPurchase');
Route::get('user_purchases', 'PurchaseController@user_purchases')->name('userPurchases');
Route::get('purchase/{purchase_id?}', 'PurchaseController@details')->name('purchaseDetails');
Route::get('back/', 'HomeController@back')->name('back');
Route::get('/paycheck/{paycheck_id?}/details', 'PaycheckController@paycheckdetail')->name('paycheckDetails');
Route::get('/helpme', 'HomeController@helpme')->name('helpme');



// Testing routes

// -----
// RH permissions
Route::group(['middleware' => ['rh']], function() {
	Route::get('/months', 'PaycheckController@months')->name('months');
	Route::get('/month/{month_id}/users', 'PaycheckController@users')->name('monthsUsers');
	Route::get('/month/{month_id?}/user/{user_id?}/paychecks', 'PaycheckController@paycheckList')->name('paycheckList');
	Route::get('paycheck/add', 'PaycheckController@create')->name('addPaycheck');
	Route::post('paycheck/add', 'PaycheckController@store')->name('storePaycheck');
	Route::get('paychecks/create', 'PaycheckController@batchCreate')->name('paychecksBatch');
	Route::post('paychecks/create', 'PaycheckController@batchCreate')->name('paychecksBatchInsert');
	Route::get('paychecks/helper', 'PaycheckController@paycheckHelper')->name('paycheckHelper');
	Route::get('paychecks/helper/{branch_id?}', 'PaycheckController@downloadPaycheckHelper')->name('downloadPaycheckHelper');
	Route::get('paychecks/month/create', 'PaycheckController@createMonth')->name('createMonth');
	Route::post('paychecks/month/create', 'PaycheckController@createMonth')->name('storeMonth');
	Route::get('/adduser', 'UserController@create')->name('createUser');
	Route::post('/adduser', 'UserController@create')->name('insertUser');
	Route::get('/users', 'UserController@list')->name('getUsers');
	Route::get('/user/{user_id?}/resetpassword', 'UserController@resetPassword')->name('resetPassword');
	Route::post('/user/{user_id?}/edit', 'UserController@updateUser')->name('editUserProfile');
	Route::get('/users/by/{type?}', 'UserController@listBy')->name('getUsersBy');
	Route::get('/users/search', 'UserController@search')->name('searchUser');
	Route::post('/users/searchresult/{name?}', 'UserController@searchResults')->name('searchUserResults');
	Route::get('/users/searchresult/{name?}', 'UserController@searchResults')->name('searchUserResults');
	Route::get('/user/{user_id?}/deactivate', 'UserController@deactivateUser')->name('deactivateUser');
	Route::get('/user/{user_id?}/activate', 'UserController@activateUser')->name('activateUser');
	Route::get('/users/disabled', 'UserController@disabledUsers')->name('disabledUsers');
	Route::get('paycheck/{paycheck_id?}/disable', 'PaycheckController@disablePaycheck')->name('disablePaycheck');
	Route::get('paycheck/{paycheck_id?}/enable', 'PaycheckController@enablePaycheck')->name('enablePaycheck');
});

// --------	
// Admin // Directors Permissions

Route::group(['middleware' => ['diretoria']], function() {
	Route::get('/admin', 'HomeController@admin')->name('adminarea');	
	Route::get('/permissiongroups', 'PermissionController@permissionGroups')->name('permissionGroups');
	Route::get('/create/permissiongroup', 'PermissionController@create')->name('createPermissionGroup');
	Route::post('/create/permissiongroup', 'PermissionController@create')->name('insertPermissionGroup');
	Route::get('permissiongroup/{role_id?}', 'PermissionController@view')->name('permissionUsers');
	Route::get('permissiongroup/{role_id?}/adduser', 'PermissionController@addUser')->name('addUserToGroup');
	Route::get('permissiongroup/{role_id?}/adduser/{user_id?}', 'PermissionController@insertUserInGroup')->name('insertUserInGroup');
	Route::get('director/approve_purchase/{purchase_id?}', 'PurchaseController@approveThirdPhase')->name('directorApproval');
	Route::get('director/deny_purchase/{purchase_id?}', 'PurchaseController@denyThirdPhase')->name('directorDenial');
	Route::get('purchases/all/{ordering?}', 'PurchaseController@allPurchases')->name('allPurchases');
	
});

// Managers permissions
Route::group(['middleware' => ['gerencia']], function() {
	Route::get('manager/purchases', 'PurchaseController@managerPurchases')->name('managerPurchases');
Route::get('manager/approve_purchase/{purchase_id?}', 'PurchaseController@approveSecondPhase')->name('managerApproval');
Route::get('manager/deny_purchase/{purchase_id?}', 'PurchaseController@denySecondPhase')->name('managerDenial');
	
});
//--------
// Financial permissions
Route::group(['middleware' => ['financeiro']], function() {
	Route::get('purchase/{purchase_id?}/pay', 'PurchaseController@payPurchase')->name('setPurchaseAsPaid');
	Route::get('purchases/topay', 'PurchaseController@purchasesToPayList')->name('toPay');
});
//--------
// General permissions
Route::get('purchase/{purchase_id?}/files', 'PurchaseController@purchaseFiles')->name('purchaseFiles');
Route::get('purchase/{purchase_id?}/files/insert', 'PurchaseController@createPurchaseFiles')->name('createPurchaseFiles');
Route::post('purchase/{purchase_id?}/files/insert', 'PurchaseController@createPurchaseFiles')->name('createPurchaseFiles');
Route::get('purchase/{purchase_id?}/file/{file_id?}/remove', 'PurchaseController@removeFile')->name('removeFile');
//--------
