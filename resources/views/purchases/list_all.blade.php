@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Todas as compras</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a class="btn btn-warning" href="{{ URL::previous() }}">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($purchases) > 0)
                    @foreach($purchases as $purchase)
                    <a href="{{ route('purchaseDetails', $purchase->id, $user->id) }}" class="list-group-item list-group-item-action">
                      {{ $purchase->title }}
                      @if($purchase->approval_level > 2)
                        <span class="badge badge-success">
                          {{$purchase->approval_message}}
                        </span>
                        @if($purchase->is_paid)
                        <span class="badge badge-info">
                          Pago
                        </span>
                        @else
                        <span class="badge badge-warning">
                          Não pago
                        </span>
                        @endif
                      @else
                        <span class="badge badge-warning">
                          {{$purchase->approval_message}}
                        </span>
                      @endif
                        
                    </a>
                    @endforeach
                    @else
                      <span> Não há nenhuma solicitação aguardando aprovação</span>
                    @endif
                  </div>
                  {{ $purchases->links() }}
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <h4>Funções</h4>
                  <a href="{{ route('allPurchases', ['field' => 'is_paid', 'order' => 'asc']) }}" class="btn btn-block btn-primary">Ordenar por pagos</a>
                  <a href="{{ route('allPurchases', ['field' => 'approval_level', 'order' => 'asc']) }}" class="btn btn-block btn-warning">Ordenar por nível de aprovação</a>
                  @if($is_ordered)
                    <a href="{{ route('allPurchases') }}" class="btn btn-block btn-success">Ordenar por data</a>
                  @endif
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary text-justify">A lista ao lado mostra todas as solicitações de compra já feitas no sistema, com seus status de aprovação e ordenados das mais recentes para as mais antigas. As que já foram aprovadas pela diretoria possuem um marcador dizendo se já foi <span class="badge badge-info">paga </span> ou <span class="badge badge-warning">não</span></p>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection