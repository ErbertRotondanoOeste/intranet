@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Detalhes da Solicitação</h1>
            @if($purchase->approval_level > 2)
            <div class="container bg-success rounded mt-3 pb-0">
            @else
            <div class="container bg-warning rounded mt-3 pb-0">
            @endif
            	<p class="text-center">
            		{{$purchase->approval_message}}	
            	</p>
            </div>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-primary">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  
                  <div class="form-group">
                    <label for="email">Título: </label>
                    <input type="email" class="form-control" value="{{ $purchase->title }}" readonly>
                  </div> 
                  <div class="form-group">
                    <label for="register">Descrição: </label>
                    <input type="text" class="form-control" value="{{ $purchase->description }}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="admission_date">Valor: 
                    	@if($purchase->is_paid)
                    		<span class="badge badge-success">pago</span>
                    	@else
                    		<span class="badge badge-warning">não pago</span>
                    	@endif
                    </label>
                    <input type="number" class="form-control" value="{{ $purchase->value }}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="branch">Filial</label>
                    <input type="text" class="form-control" value="{{ $branch }}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="purchase_owner">Solicitante</label>
                    <input type="text" class="form-control" name="purchase_owner" value="{{ $purchase_owner->name }}" readonly>
                  </div>

              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-12 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <div class="row d-flex justify-content-center mb-3">
                  <h4>Funções</h4>
                </div>
                <div class="row mb-2">
                    <a href="{{ route('purchaseFiles', $purchase->id) }}" class="btn btn-primary btn-block">Arquivos</a>
                </div>
                @if($purchase->approval_level == 1 && ($user->role == 'diretoria' || $user->role == 'admin' || $user->role == 'gerencia'))
                <div class="row mb-2">
                  <div class="col-md-6">
                    <a href="{{ route('managerApproval', $purchase->id) }}" id="second_phase_approval" class="btn btn-warning btn-block">Aprovar para 2ª fase</a>
                  </div>
                  <div class="col-md-6">
                    <a href="{{ route('managerDenial', $purchase->id) }}" id="second_phase_denial" class="btn btn-danger btn-block">Reprovar</a>
                  </div>
                </div>
                @endif
                @if($user->role == 'diretoria' || $user->role == 'admin')
                  @if($purchase->approval_level < 3 && $purchase->approval_level > 0)
                    <div class="row mb-2">
                      <div class="col-md-6">
                        <a href="{{ route('directorApproval', $purchase->id) }}" id="third_phase_approval" class="btn btn-success btn-block">Aprovar para pagamento</a>
                      </div>
                      <div class="col-md-6">
                        <a href="{{ route('directorDenial', $purchase->id) }}" id="third_phase_approval" class="btn btn-danger btn-block">Negar fase de pagamento</a>
                      </div>
                    </div>
                  @endif
                @endif
                @if(!$purchase->is_paid && $purchase->approval_level == 3)
                <div class="row mb-2">
                    <a href="{{ route('setPurchaseAsPaid', $purchase->id) }}" id="pay_btn" class="btn btn-info btn-block">Marcar como pago</a>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <script type="text/javascript">

      const second_phase  = document.getElementById('second_phase_approval');
      const third_pase    = document.getElementById('third_phase_approval');
      const pay_btn       = document.getElementById('pay_btn');
      second_phase.addEventListener('click', (e) => {
        if(confirm('Essa solicitação será enviada para a aprovação da diretoria. Você deseja prosseguir?')){
          return true;
        } else {
          e.preventDefault();
        }

      });
       third_pase.addEventListener('click', (e) => {
        if(confirm('Essa solicitação será enviada para o financeiro para pagamento. Você deseja prosseguir?')){
          return true;
        } else {
          e.preventDefault();
        }

      });
       pay_btn.addEventListener('click', (e) => {
        if(confirm('Você realmente deseja marcar essa solicitação como paga?')){
          return true;
        } else {
          e.preventDefault();
        }

      });
      
    </script>
@endsection