@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Solicitações de compras aguardando aprovação</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a class="btn btn-primary" href="{{ route('allPurchases') }}">Listar todas as solicitações</a>
          </div>
          <div class="col-sm-6 float-right text-right">
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($purchases) > 0)
                    @foreach($purchases as $purchase)
                    <a href="{{ route('purchaseDetails', $purchase->id, $user->id) }}" class="list-group-item list-group-item-action">
                      {{ $purchase->title }}
                      @if($purchase->approval_level > 2)
                        <span class="badge badge-success">
                      @else
                        <span class="badge badge-warning">
                      @endif
                        {{$purchase->approval_message}}
                        </span>
                    </a>
                    @endforeach
                    @else
                      <span> Não há nenhuma solicitação aguardando aprovação</span>
                    @endif
                  </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary text-justify">A lista ao lado apresenta somente as solicitações de compra que estão em Estágio de Aprovação <span class="badge badge-success">2</span> Isso significa que os gerentes das lojas já aprovaram a primeira fase e para que a solicitação seja paga pelo setor financeiro a diretoria precisa aprovar para o Estágio de Aprovação <span class="badge badge-success">3</span>. Clique no nome da solicitação para ver o botão de aprovação.</p>
                  <p class="text-secondary text-justify">O botão acima dessa caixa lhe mostra todas as solicitações.</p>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection