@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Suas solicitações de compras</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ route('createPurchase') }}" class="btn btn-primary ">Solicitar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($purchases) > 0)
                    @foreach($purchases as $purchase)
                    <a href="{{ route('purchaseDetails', $purchase->id, $user->id) }}" class="list-group-item list-group-item-action">
                      {{ $purchase->title }}
                      @if($purchase->approval_level > 2)
                        <span class="badge badge-success">
                          {{$purchase->approval_message}}
                        </span>
                        @if($purchase->is_paid)
                          <span class="badge badge-success">
                            PAGO
                          </span>
                        @endif
                      @else
                        <span class="badge badge-warning">
                          {{$purchase->approval_message}}
                        </span>
                      @endif
                    </a>
                    @endforeach
                    @else
                      <span> Não há nenhuma solicitação cadastrada</span>
                    @endif
                  </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary text-justify">A lista ao lado apresenta todas as suas solicitações de compra e os Estágios de Aprovação das mesmas. Clique na solicitação para ver mais detalhes e anexar arquivos.</p>
                  <p class="text-secondary text-justify">Caso deseje cadastrar uma nova solicitação, use o botão acima.</p>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection