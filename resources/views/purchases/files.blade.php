@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Arquivos da Solicitação de Compras</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
            <a href="{{ route('createPurchaseFiles', $purchase->id) }}" class="btn btn-primary">Adicionar novo</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($files) > 0)
                    @foreach($files as $file)
                    <form action="{{ route('downloadFile') }}" method="POST">
                      @csrf
                      <div class="row">
                      <div class="col-md-11">
                      <input type="hidden" name="file_id" value={{$file->id}}>
                      <input type="submit" class="list-group-item list-group-item-action" value="{{ $file->name }}  ">
                      </div>
                      <div class="col-md-1 pt-3">
                        <a href="{{ route('removeFile', [$purchase->id, $file->id]) }}">
                          <span class="badge badge-danger">x</span>
                        </a>
                      </div>
                      </div>
                    </form>
                    
                    @endforeach
                    @else
                      <span> Não há nenhum arquivo cadastrado para essa solicitação</span>
                    @endif
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">Clique no nome do arquivo ao lado para baixá-lo. Caso deseje adicionar um novo arquivo use o botão acima.</p> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection