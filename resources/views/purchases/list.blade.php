@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Todas as compras</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
          </div>
          <div class="col-sm-6 float-right text-right">
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($purchases) > 0)
                    @foreach($purchases as $purchase)
                    <a href="{{ route('purchaseDetails', $purchase->id, $user->id) }}" class="list-group-item list-group-item-action">
                      {{ $purchase->title }}
                      @if($purchase->is_paid)
                        <span class="badge badge-info">
                          Pago
                        </span>
                        @else
                        <span class="badge badge-warning">
                          Não pago
                        </span>
                        @endif
                    </a>
                    @endforeach
                    @else
                      <span> Não há nenhuma solicitação aguardando pagamento</span>
                    @endif
                  </div>
                  {{ $purchases->links() }}
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary text-justify">A lista ao lado mostra todas as solicitação que foram aprovadas pela diretoria e estão aguardando pagamento. Os arquivos fiscais podem ser baixados na tela de detalhes da solicitação. Para isso, clique no nome da solicitação e depois no botão <span class="badge badge-primary">Arquivos</span>.</p>
                </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection