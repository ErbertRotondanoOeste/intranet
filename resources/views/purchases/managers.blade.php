@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Solicitações de compras da sua filial</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @foreach($purchases as $purchase)
                    <a href="{{ route('purchaseDetails', $purchase->id, $user->id) }}" class="list-group-item list-group-item-action">
                      {{ $purchase->title }}
                      @if($purchase->approval_level > 2)
                      <span class="badge badge-success">
                      @else
                      <span class="badge badge-warning">
                      @endif
                        {{$purchase->approval_message}}
                      </span>
                    </a>
                    @endforeach
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection