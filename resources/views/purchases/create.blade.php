@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Cadastrar solicitação de compra</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form method="POST">
                  @csrf
                  <div class="form-group">
                    <label for="user">Título: </label>
                    <input class="form-control" type="text" name="title" placeholder="Exemplo: Solicitação para compra de material" required>
                  </div>
                  <div class="form-group">
                    <label for="user">Descrição: </label>
                    <input class="form-control" type="textarea" name="description" placeholder="Exemplo: Estou solicitando a compra do material X devido a necessidade para o projeto Y" required>
                  </div>
                  <div class="form-group">
                    <label for="user">Valor Total: </label>
                    <input class="form-control" type="number" name="value" step="0.01" placeholder="87,28" required>
                  </div>
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Salvar Solicitação</button>
                </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Insira o título, descrição e valor total da sua solicitação de compra. Ao salvar, a solicitação será encaminhada para o gerente responsável para ser avaliada.
                  </p>
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection