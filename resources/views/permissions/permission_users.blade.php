@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Usuários do Grupo: {{ $role->name }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
            <a href="{{ route('addUserToGroup', [$role->id] )}}" class="btn btn-primary">Adicionar usuário ao grupo</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @foreach($users as $user)
                    <a href="{{ route('profile', [$user->id]) }}" class="list-group-item list-group-item-action">
                      {{ $user->name }}
                    </a>
                    @endforeach
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection