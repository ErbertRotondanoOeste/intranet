@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Inserindo grupo de permissão</h1>
          </div><!-- /.col -->
          <div class="col-md-6 text-right float-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form action="{{ route('insertPermissionGroup') }}" method="POST">
                  @csrf
                  <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="nome-do-grupo" required>
                  </div>
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Salvar</button>
              </form>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">No campo ao lado, insira somente letras minúsculas, sem acento e separads por hífens. Por exemplo, caso queira cadastrar o grupo de permissões: Assistência de Compras use <span class="badge badge-primary">assistencia-de-compras</span>.</p>
              </div>
            </div>
          </div>
          <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection