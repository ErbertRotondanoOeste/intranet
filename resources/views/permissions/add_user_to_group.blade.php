@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0">Usuários a serem adicionados no grupo: {{ $role->name }}</h4>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                      @foreach($users as $user)
                      <a href="{{ route('insertUserInGroup', [$role->id, $user->id]) }}" name="user" class="list-group-item list-group-item-action" >{{ $user->name }}</a>
                      @endforeach
                  </div>
              </div>
            </div>  
          </div>
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                  <h4>Instruções</h4>
                    <p class="text-secondary">Por enquanto podemos selecionar apenas um usuário por vez para adicionar a um grupo de permissão. Basta clicar no nome e confirmar que o usuário será adicionado ao grupo: <span class="badge bg-primary">{{ $role->name }}</span></p>
                  </div>
              </div>
            </div>  
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <script type="text/javascript">

      const users_nodes = document.getElementsByName('user');
      const users = Array.from(users_nodes);

      users.map((user) => {
        user.addEventListener('click', (e) => {
          const confirm_text = "Você tem certeza que deseja adicionar esse usuário ao grupo referido?"
          if (confirm(confirm_text)) {
            return true;
          } else {
            e.preventDefault();
          }

        });
      });
      
    </script>
@endsection