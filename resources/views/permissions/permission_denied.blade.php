@extends('partials.panel') 
@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row my-5">
          <div class="col-sm-6 offset-3 text-center">
            <h1 class="m-0 mb-3">Você não possui acesso a essa área</h1>
            <a href="{{ URL::previous() }}">
            	<button type="submit" class='btn btn-success btn-lg btn-block'>Voltar</button>
            </a>
            </form>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection