@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Holerites</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
            <a href="#" class="btn btn-primary">Adicionar novo</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($paychecks) > 0)
                    @foreach($paychecks as $paycheck)
                    <a href="{{ route('paycheckDetails', [$paycheck->id]) }}" class="list-group-item list-group-item-action">
                      {{ $paycheck->title }}
                      @if($paycheck->is_signed)
                      @else
                        <span class="badge badge-warning float-right">Não assinado</span>
                      @endif
                      @if(!$paycheck->is_active)
                        <span class="badge badge-danger float-right">Inativo</span>
                      @endif
                    </a>
                    @endforeach
                    @else
                      <span>Não há nenhum holerite cadastrado</span>
                    @endif
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">A listagem ao lado apresenta os nomes dos arquivos cadastrados como holerites para esse usário. Clique para visualizar detalhes, caso o holerite não esteja assinado haverá um alerta <span class="badge badge-warning">amarelo</span>.</p> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection