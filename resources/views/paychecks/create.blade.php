@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Adicionar novo Holerite</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form method="POST" action="{{ route('storePaycheck') }}" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="user">Funcionário: </label>
                    <select class="form-control" name="user_id" id="user" required>
                        <option>Selecione: </option>
                        @foreach($users as $user)
                          <option value="{{ $user->id }}">{{$user->name}}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="month">Competência</label>
                    <select class="form-control" name="month_id" id="month" required>
                      <option>Selecione: </option>
                      @foreach($months as $month)
                        <option value="{{ $month->id }}">{{ $month->reference }}</option>
                      @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="chooseFile">Arquivo do Holerite</label>
                    <input type="file" name="file" class="form-control-file" id="chooseFile">
                  </div> 
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Salvar Holerite</button>
              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Escolha o funcionário, a competência e envie o suba o arquivo necessário. Não se esqueça de nenhum desses passos!
                  </p>
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection