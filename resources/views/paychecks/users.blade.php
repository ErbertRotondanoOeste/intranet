@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Holerites de {{ $month->reference }} </h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($users) > 0)
                    @foreach($users as $user)
                    <a href="{{ route('paycheckList', [$month->id, $user->id]) }}" class="list-group-item list-group-item-action">
                      {{ $user->name }}
                      @if($user->is_signed)
                      @else
                        <span class="badge badge-warning float-right">Possui holerites a assinar</span>
                      @endif
                    </a>

                    @endforeach
                    @else
                      <span> Não há nenhum Holerite cadastrado</span>
                    @endif
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">Agora escolha de qual funcionário deseja ver o holerite. Caso o funcionário não tenha assinado ainda aparecerá um <span class="badge badge-warning">alerta</span> ao lado do seu nome.</p> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection