@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Adicionar novo Mês</h1>
          </div><!-- /.col --> 
          <div class="col-sm-6 text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form method="POST" action="{{ route('storeMonth') }}" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="user">Mês de referência: </label>
                    <select class="form-control" name="month_number" id="user" required>
                        <option>Selecione: </option>
                        <option value="01">Janeiro</option>
                        <option value="02">Fevereiro</option>
                        <option value="03">Março</option>
                        <option value="04">Abril</option>
                        <option value="05">Maio</option>
                        <option value="06">Junho</option>
                        <option value="07">Julho</option>
                        <option value="08">Agosto</option>
                        <option value="09">Setembro</option>
                        <option value="10">Outubro</option>
                        <option value="11">Novembro</option>
                        <option value="12">Dezembro</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="month">Ano</label>
                    <input class="form-control" type="number" min="2015" max="9999" name="year" value="{{ $year }}">
                  </div>
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Salvar mês</button>
              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Selecione o mês e edite o ano caso necessário. 
                  </p>
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection