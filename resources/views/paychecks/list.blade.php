@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Meses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
            <a href="#" class="btn btn-primary">Adicionar novo</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($paychecks > 0))
                    @foreach($paychecks as $paycheck)
                    <a href="{{ route('monthsUsers', [$month->id]) }}" class="list-group-item list-group-item-action">
                      {{ $month->name }}
                    </a>
                    @endforeach
                    @else
                      <span>Não há nenhum mês cadastrado</span>
                    @endif
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">Escolha o mês de competência do holerite ao lado.</p> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection