@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Holerite</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="row">
              <div class="col-md-6">
                <div class="card">
                  <div class="card-body">
                    <h4>Mês</h4>
                    <p class="text-secondary">Abaixo seguem informações sobre o mês</p> 
                    <p><strong>Mês</strong>: {{ $month->name }}</p>
                    <p><strong>Competência</strong>: {{ $month->reference }}</p>
                  </div>
                </div>
              </div>  
              <div class="col-md-6">
                <div class="card">
                  <div class="card-body">
                    <h4>Funcionário</h4>
                    <p><strong>Nome</strong>: {{ $user->name }}</p>
                    <p><strong>Número de Registro</strong>: {{ $user->register }}</p>
                    <p><strong>Email</strong>: {{ $user->email }}</p>
                  </div>
                </div>
              </div>  
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-body">
                    <h4>Holerite</h4>
                    @if(!$paycheck->is_active)
                    <div class="row bg-danger rounded mb-2">
                      <div class="col-md-8 offset-2">
                        <p class="text-light pt-3 text-center">
                          <strong>Esse holerite está inativo</strong>
                        </p>
                      </div>
                    </div>
                    @endif
                    @if($paycheck->is_signed)
                      <p><strong>Assinado em</strong>: {{ $paycheck->signed_at }}</p>
                      <div class="row">
                        <div class="col-md-12">
                            <form method="POST" action="{{ route('downloadFile') }}">
                              @csrf
                              <input type="hidden" name="file_id" value="{{$file->id}}">
                              <button type="submit" class="btn btn-primary btn-block btn-lg">Baixar Holerite</button>
                            </form>
                          </div>
                      </div>
                    @else
                      <div class="row bg-warning rounded">
                        <div class="col-md-8 offset-2">
                          <p class="text-light pt-3 text-center">
                            <strong>Esse holerite ainda não foi assinado</strong>
                          </p>
                        </div>
                      </div>
                      <hr />
                        <div class="row mt-3">
                          @if($paycheck->user_id == Auth::id())
                          <div class="col-md-6">
                            <form method="POST" action="{{ route('downloadFile') }}">
                              @csrf
                              <input type="hidden" name="file_id" value="{{$file->id}}">
                              <button type="submit" class="btn btn-primary btn-block btn-lg">Baixar Holerite</button>
                            </form>
                          </div>
                          <div class="col-md-6">
                              <a href="{{route('signPaycheck', [$paycheck->id])}}" class="btn btn-success btn-block btn-lg">Assinar Holerite</a>
                          </div>
                          @else
                          <div class="col-md-12">
                            <form method="POST" action="{{ route('downloadFile') }}">
                              @csrf
                              <input type="hidden" name="file_id" value="{{$file->id}}">
                              <button type="submit" class="btn btn-primary btn-block btn-lg">Baixar Holerite</button>
                            </form>
                          </div>
                          @endif

                        </div>
                    @endif
                    <div class="row mt-2"> 
                      <div class="col-md-12">
                        @if($is_privileged_user)
                          @if($paycheck->is_active)
                            <a href="{{ route('disablePaycheck', [$paycheck->id]) }}" class="btn btn-danger btn-block btn-lg">Inativar Holerite</a>
                            @else
                            <a href="{{ route('enablePaycheck', [$paycheck->id]) }}" class="btn btn-success btn-block btn-lg">Ativar Holerite</a>
                          @endif
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
            </div>
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">A sua esquerda estão as informações sobre o mês, funcionário e holerite. Caso o funcionário ainda não tenha feito a assinatura haverá um alerta <span class="badge bg-warning">amarelo</span>.</p>
                @if(!$paycheck->is_active)
                  <p class="text-secondary">Esse holerite está <span class="badge bg-danger">inativo</span>, entre em contato com o RH para entender os motivos.</p>
                @endif
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection