@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Resultados</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                @if(sizeof($errors) > 0)
                  @foreach($errors as $error)
                    <div class="list-group">
                      <span class="list-group-item list-group-item-danger">{{ $error['user'] }} : {{ $error['message']  }}
                      </span>
                    </div>
                  @endforeach
                @else
                  <div class="list-group">
                    <span class="list-group-item list-group-item-success">Todos os holerites enviados sem nenhum erro!</span>
                  </div>
                @endif
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                @if(sizeof($errors) > 0)
                  <p class="text-secondary">Alguns erros aconteceram, verifique os erros e certifique-se de que os nomes dos arquivos estão batendo com os nomes dos usuários no sistema. Preste atenção aos acentos e espaços e tente novamente. Os que não apresentaram erros foram enviados normalmente. Então na sua próxima tentativa envie somente os que apresentaram erros.</p> 
                  <a href="{{ URL::previous() }}" class="btn btn-block btn-warning">Tentar novamente</a>
                @else
                  <p class="text-secondary">Deu tudo certo, sendo assim verifique alguns funcionários para ver se os arquivos de holerites estão conforme esperado.</p> 
                  <div class="row">
                    <a href="{{route('months')}}" class="btn btn-block btn-success">Holerites</a>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection