@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Meses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @foreach($months as $month)
                    <a href="{{ route('monthsUsers', [$month->id]) }}" class="list-group-item list-group-item-action">
                      {{ $month->name }} - {{ $month->reference }}
                    </a>
                    @endforeach
                  </div>
              </div>
            </div>
            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">Escolha o mês de competência do holerite ao lado. Caso o mês que você deseja não apareça, cadastre um usando o botão verde abaixo.</p> 
              </div>
            </div>
            <div class="card">
              <div class="card-body">
                <h4>Funções</h4>
                  <a href="{{ route('addPaycheck') }}" class="btn btn-primary btn-block">Adicionar Holerite Único</a>
                  <a href="{{ route('paychecksBatch') }}" class="btn btn-warning text-white btn-block">Adicionar Holerites em Lote</a>
                  <a href="{{ route('createMonth') }}" class="btn btn-success btn-block">Adicionar novos meses</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection