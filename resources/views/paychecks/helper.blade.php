@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Assistente de renomeação</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-8 -->
          
          <div class="col-lg-6">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Esse assistente consiste apenas em um arquivo que irá renomear todos os PDFs em ordem alfabética para ajudar no envio dos Holerites. Sendo assim, certifique-se de ter usado o software de divisão de pdf para que o assistente funcione adequadamente.
                  </p>
                  <p class="text-secondary">
                    Para usá-lo, escolha de qual das filiais você deseja baixar e coloque-o na pasta onde estão os PDFs cortados. Depois basta dar 2 cliques no assistente e a renomeação acontecerá. O PDF de origem deve ter o nome <span class="badge badge-info">Holerite</span>.
                  </p>
                  <p class="text-secondary">
                    Após a renomeação verifique <span class="badge badge-info">3</span> arquivos para saber se tudo funcionou conforme o esperado. O ideal é que seja o primeiro arquivo, um arquivo do meio e o último arquivo. Compare o nome do arquivo com o Holerite e veja se os funcionários batem. Só depois de ter certeza que tudo está conforme esperado, prossiga com o envio dos Holerites. Caso os nomes dos arquivos não batam com os Holerites, duas coisas são prováveis:
                    <ol>
                      <li class="text-secondary">Um dos funcionários possui mais de uma folha de holerite;</li>
                      <li class="text-secondary">Os nomes dos funcionários não estão atualizados no intranet.</li>
                    </ol>
                  </p>
                  <p class="text-secondary">
                    <span class="badge badge-info">Suporte apenas para Windows</span>
                  </p>
                </div>
              </div>
          </div>
          <div class="col-lg-6">
            <div class="card">
              <div class="card-body">
                @foreach($branches as $branch)
                  <a class="btn btn-lg btn-block btn-info" href="{{ route('downloadPaycheckHelper', [$branch->id]) }}">Assistente de {{$branch->name}}</a>
                @endforeach
              <a class="btn btn-lg btn-block btn-danger" href="{{ route('paychecksBatch') }}">Voltar</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection