@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Adicionar Holerites</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
            <a href="{{ route('paycheckHelper') }}" class="btn btn-success">Assistente de renomeação</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form method="POST" action="{{ route('paychecksBatchInsert') }}"enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="month">Competência</label>
                    <select class="form-control" name="month_id" id="month" required>
                      <option value="">Selecione: </option>
                      @foreach($months as $month)
                        <option value="{{ $month->id }}">{{ $month->reference }}</option>
                      @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="chooseFiles">Arquivos de Holerite</label>
                    <input type="file" name="files[]" class="form-control-file" id="chooseFiles" multiple required>
                  </div> 
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Enviar Holerites</button>
              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Escolha a competência dos Holerites enviados e selecione os múltiplos arquivos que deseja enviar.
                    <span class="badge badge-warning">Atenção!</span> Os nomes dos arquivos devem estar exatamente iguais aos nomes dos usuários no sistema para que a vinculação seja feita da forma adequada. É recomendado o uso do assistende de renomeação encontrado no botão acima.
                  </p>
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection