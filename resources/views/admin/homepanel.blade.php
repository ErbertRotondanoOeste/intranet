@extends('partials.panel') 
@section('content')
	<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Painel</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card card-danger card-outline">
              <div class="card-header">
                <h5 class="m-0">Solicitações de Compra</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Cadastre uma solicitação de compra a ser aprovada pelo seu gerente e diretoria.</p>
                <a href="{{ route('createPurchase') }}" class="btn btn-danger">Nova solicitação de compra</a>
              </div>
            </div>
          </div>
           <div class="col-lg-6">
            <div class="card card-{{ $classname }} card-outline">
              <div class="card-header">
                <h5 class="m-0">Holerites 
                  @if($any_to_sign)
                  <span class="badge badge-{{ $classname }} text-white">Você tem holerites pendentes</span>
                  @endif
                </h5>
              </div>
              <div class="card-body">
                <p class="card-text">Visualize e assine os seus holerites</p>
                <a href="{{ route('myPaychecks') }}" class="btn btn-{{ $classname }}">Holerites</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-success card-outline">
              <div class="card-header">
                <h5 class="m-0">Precisa de ajuda?</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Veja nossos vídeos, e entenda o funcionamento do sistema</p>
                <a href="{{ route('helpme') }}" class="btn btn-success btn-lg">Vídeos</a>
              </div>
            </div>
          </div>
        </div>
        @if($role == 'diretoria' || $role == 'admin')
        {{-- RH cards --}}
        <hr />
        <h4>Diretoria</h4>
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-secondary card-outline">
              <div class="card-header">
                <h5 class="m-0">Funcionários por setor</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Veja os funcionários listados por setor</p>
                <a href="{{ route('getUsersBy', 'department') }}" class="btn btn-secondary">Funcionários</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
            <div class="card card-warning card-outline">
              <div class="card-header">
                <h5 class="m-0">Solicitações de compras a serem aprovadas</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Gerencie as solicitações de compras que estão aguardando a sua aprovação.</p>
                <a href="{{ route('managerPurchases') }}" class="btn btn-warning">Solicitações</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        @endif
        <!-- /.row -->
        @if($role == 'rh' || $role == 'admin' || $role == 'diretoria')
        {{-- RH cards --}}
        <hr />
        <h4>RH</h4>
        <div class="row">
          <!-- /.col-md-4 -->
          <div class="col-lg-4">
            <div class="card card-success card-outline">
              <div class="card-header">
                <h5 class="m-0">Assistente de renomeação</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Baixe o assistente de renomeação para usá-lo nos arquivos dos holerites.</p>
                <a href="{{ route('paycheckHelper') }}" class="btn btn-success">Ir para o assistente</a>
              </div>
            </div>
          </div>
           <div class="col-lg-4">
            <div class="card card-info card-outline">
              <div class="card-header">
                <h5 class="m-0">Funcionários por filial</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Veja os funcionários listados por filial</p>
                <a href="{{ route('getUsersBy', 'branch') }}" class="btn btn-info">Funcionários</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-4 -->
          <div class="col-lg-4">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h5 class="m-0">Buscar funcionário</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Busque um funcionário pelo nome</p>
                <a href="{{ route('searchUser') }}" class="btn btn-primary">Buscar</a>
              </div>
            </div>
          </div>
        </div>
        @endif
        <!-- /.row -->
         @if($role == 'financeiro' || $role == 'admin' || $role == 'diretoria')
        {{-- RH cards --}}
        <hr />
        <h4>Financeiro</h4>
        <div class="row">
          <!-- /.col-md-4 -->
          <div class="col-lg-6">
            <div class="card card-warning card-outline">
              <div class="card-header">
                <h5 class="m-0">Solicitações de compras a serem pagas</h5>
              </div>
              <div class="card-body">
                <p class="card-text">Visualize as solicitações de compras que estão aprovadas para pagamento.</p>
                <a href="{{ route('toPay') }}" class="btn btn-warning">Solicitações</a>
              </div>
            </div>
          </div>
        </div>
        @endif
        <!-- /.row -->


      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection