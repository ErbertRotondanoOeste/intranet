@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Buscar Usuário</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form method="POST" action={{ route('searchUserResults') }}>
                	@csrf
                  <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name" placeholder="Usuário da Silva" name="name" required>
                  </div>
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Buscar usuário</button>
              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Digite o nome do usuário e clique em buscar, em futuras versões disponibilizaremos a busca por outros parâmetros.
                  </p>
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection