@extends('layouts.app') 
@section('content')

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-header text-center"><img src="https://yata-apix-61589086-5007-4240-9eec-744605e3805f.lss.locawebcorp.com.br/7d4eff6626864e6ab5be2ae448c898df.png" width="200"></div>
              <div class="card-body">
                <div class="text-center mb-4 px-5 bg-alert">
                  <h5>Instruções</h5>
                  @if($is_first_login)
                        <span class="small text-secondary"> Esse é o seu primeiro login na plataforma, por conta disso, se faz necessário alterar a senha padrão para uma que você se lembre.</span>
                  @else
                        <span class="small text-secondary"> Insira a sua nova senha, certifique-se de que não seja igual a antiga e que a confirmação bata com a senha.</span>
                  @endif
                </div>
                <form id="form" method="POST" action={{ route('updatePassword') }}>
                  @csrf
                  <div class="form-group">
                    <label for="password">Senha: </label>
                    <input type="password" class="form-control" id="password" placeholder="********" name="password" required>
                  </div>
                  <div class="form-group">
                    <label for="password">Confirmação de senha: </label>
                    <input type="password" class="form-control" id="password_confirmation" placeholder="********" name="password_confirmation" required>
                  </div>
                  <button type="submit" class="btn btn-secondary btn-block">Salvar nova senha</button>
                  </form>
                  @if($is_first_login)
                    <form action="{{ route('logout') }}" method="POST">
                      @csrf
                      <button type="submit" class="btn btn-danger btn-block mt-1">Sair</button>
                    </form>
                  @else 
                    <a class="btn btn-primary btn-block" href="{{ route('home') }}"> Voltar</a>
                  @endif
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          </div>


      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <script type="text/javascript">
      const pass = document.getElementById('password');
      const confirm = document.getElementById('password_confirmation');
      const form = document.getElementById('form');

      form.addEventListener('submit', (e) => {
        e.preventDefault();
        
        if(pass.value == confirm.value){
          form.submit()
        } else {
          alert("As senhas não batem");
          confirm.classList.add('bg-warning')
        }
      })

    </script>

@endsection