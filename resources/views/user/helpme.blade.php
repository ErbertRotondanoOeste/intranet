@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Vídeos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Nessa tela temos os vídeos tutoriais de acordo ao seu grupo de permissão, os botões lhe redirecionarão para o youtube.
                  </p>
                </div>
              </div>
          </div>
        </div>
        <div class="row">
          @if($admin || $diretoria)
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                <a href="https://youtube.com/playlist?list=PLeLFxMETc3Jo2OLXMSAmYlnm-QX-0EXjg" class="btn btn-block btn-lg btn-secondary" target="_blank">Playlist de vídeos da Diretoria</a>
              </div>
            </div>
          </div>
          @endif
          @if($rh)
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                <a href="https://youtube.com/playlist?list=PLeLFxMETc3JqeP9MgU5D9Ekn9_NOsB-vb" class="btn btn-block btn-lg btn-danger" target="_blank">Playlist de vídeos do RH</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-4 -->
          @endif
          @if($gerencia)
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                <a href="https://youtube.com/playlist?list=PLeLFxMETc3Jq0F2Tid6h1vR9QRb0peIjH" class="btn btn-block btn-lg btn-info" target="_blank">Playlist de vídeos da Gerência</a>
              </div>
            </div>
          </div>
          @endif
        </div>
        <div class="row">
          @if($financeiro)
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                <a href="https://youtube.com/playlist?list=PLeLFxMETc3Jr-JSzw8LcwBKul1TU0kM0m" class="btn btn-block btn-lg btn-success" target="_blank">Playlist de vídeos do Financeiro</a>
              </div>
            </div>
          </div>
          @endif
          <div class="col-lg-4">
            <div class="card">
              <div class="card-body">
                <a href="https://youtube.com/playlist?list=PLeLFxMETc3JqtHQfpDw3HE0bGPbnTgGf_" class="btn btn-block btn-lg btn-primary" target="_blank">Playlist de vídeos Gerais</a>
              </div>
            </div>
          </div>
          <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection