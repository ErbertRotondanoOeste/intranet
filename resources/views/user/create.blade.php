@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Adicionar novo usuário</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form method="POST" action={{ route('insertUser') }}>
                  @csrf
                  <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" id="name" placeholder="Usuário da Silva" name="name">
                  </div>
                  <div class="form-group">
                    <label for="email">Email: </label>
                    <input type="email" class="form-control" id="email" placeholder="usuário@oestetecnologia.com" name="email">
                  </div> 
                  <div class="form-group">
                    <label for="register">Número de folha: </label>
                    <input type="number" class="form-control" name="register" id="register" placeholder="123" >
                  </div>
                  <div class="form-group">
                    <label for="admission_date">Data de Admissão: </label>
                    <input type="date" class="form-control" id="admission_date" name="admission_date">
                  </div>
                  <div class="form-group">
                    <label for="department">Setor: </label>
                    <select class="form-control" name="department" id="department" required>
                        <option>Selecione: </option>
                        @foreach($departments as $department)
                          <option value="{{ $department->id }}">{{$department->name}}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="branch">Filial</label>
                    <select class="form-control" name="branch" id="branch" required>
                      <option>Selecione: </option>
                        @foreach($branches as $branch)
                          <option value="{{ $branch->id }}">{{$branch->name}}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="role">Grupo de Permissões</label>
                     <select class="form-control" name="role" id="role" required>
                      <option>Selecione: </option>
                        @foreach($roles as $role)
                          <option value="{{ $role->id }}">{{$role->name}}</option>
                        @endforeach
                      </select>

                  </div>
                  <button type="submit" class="btn btn-secondary btn-lg btn-block">Salvar usuário</button>
              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Para o cadastro de usuário, verifique se o email está de acordo ao esperado, email e número de folha não podem ser repetidos.
                  </p>
                  @if ($errors->any())
                      <div class="alert alert-danger">
                        Register se refere a Número de Folha
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

@endsection