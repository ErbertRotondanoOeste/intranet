@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Meses</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">

          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($months) > 0)
                      @foreach($months as $month)
                      <a href="{{route('myPaychecks', [$month->id, $user->id])}}" class="list-group-item list-group-item-action">
                        {{ $month->name }} - {{ $month->reference }}
                        @if($month->is_signed)
                        @else
                        <span class="badge badge-warning float-right">Possui holerites a assinar</span>
                        @endif
                      </a>
                      @endforeach
                    @else
                      <span>Não há nenhum mês com holerite para esse funcionário</span>
                    @endif
                  </div>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <h4>Instruções</h4>
                <p class="text-secondary">Escolha o mês de competência do holerite ao lado.</p> 
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection