@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Usuários por {{$data->typename}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($data) > 0)
                    @foreach($data as $row)
                    <li class="list-group-item bg bg-info">
                      {{$row->name}}
                    </li>
                      @foreach($row->users as $user)
                        <a href="{{ route('profile', [$user->id]) }}" class="list-group-item list-group-item-action pl-5">
                          {{ $user->name }}
                        </a>
                      @endforeach
                    @endforeach
                    @else
                      <span> Não há nenhum usuário cadastrado</span>
                    @endif
                  </div>
              </div>
            </div>
          </div>
          <!-- /.col-md-8 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <div class="row d-flex justify-content-center mb-3">
                  <h4>Funções</h4>
                </div>
                <div class="row mb-2">
                  <div class="col-md-6">
                    <a href="{{ route('getUsersBy', 'branch') }}" class="btn btn-primary btn-block">Ordenar por filial</a>
                  </div>
                  <div class="col-md-6">
                    <a href="{{ route('getUsersBy', 'department') }}" class="btn btn-warning btn-block">Ordenar por setor</a>
                  </div>
                  
                </div>
                <div class="row mb-2">
                  <div class="col-md-6">
                    <a href="{{ route('getUsers') }}" class="btn btn-danger btn-block">Listar todos</a>
                  </div>
                  <div class="col-md-6">
                    <a href="{{ route('searchUser') }}" class="btn btn-secondary btn-block">Buscar</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <a href="{{ route('createUser')}}" class="btn btn-success btn-block">Adicionar novo</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection