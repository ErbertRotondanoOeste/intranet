@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Funcionários encontrados</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ route('createUser')}}" class="btn btn-primary">Adicionar novo</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                  <div class="list-group">
                    @if(sizeof($users) > 0)
                    @foreach($users as $user)
                    <a href="{{ route('profile', [$user->id]) }}" class="list-group-item list-group-item-action">
                      {{ $user->name }}
                      @if($user->is_active)
                      @else
                        <span class="badge bg-danger">inativo</span>
                      @endif
                    </a>
                    @endforeach
                    @else
                      <span> Nenhum usuário encontrado</span>
                    @endif
                  </div>
              </div>
              <div class="row d-flex justify-content-center">
                {{ $users->links() }} 
              </div>
            </div>
          </div>
          <!-- /.col-md-8 -->
          <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <h4>Instruções</h4>
                  <p class="text-secondary">
                    Clique no nome do usuário para visualizar o seu perfil.
                  </p>
                </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection