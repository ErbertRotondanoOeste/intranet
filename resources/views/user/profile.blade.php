@extends('partials.panel') 
@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Perfil</h1>
            @if($user->is_active)
            @else
            <div class="col-sm-6">
              <span class="badge bg-danger">Inativo</span>
            </div>
            @endif
          </div><!-- /.col -->
          <div class="col-sm-6 float-right text-right">
            <a href="{{ URL::previous() }}" class="btn btn-warning">Voltar</a>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <form id="form" method="POST" action="{{ route('editUserProfile', [$user->id]) }}">
                  @csrf
                  <div class="form-group">
                    <label for="name">Nome: </label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nome do Fulano da Silva" value="{{ $user->name }}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="cpf">CPF: </label>
                    <input type="text" class="form-control" id="cpf" name="cpf" value="{{ $user->cpf }}" readonly>
                  </div> 
                  <div class="form-group">
                    <label for="email">Email: </label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" readonly>
                  </div> 
                  <div class="form-group">
                    <label for="register">Registro no sistema: </label>
                    <input type="number" class="form-control" id="register" name="register" value="{{ $user->register }}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="admission_date">Data de Admissão: </label>
                    <input type="date" class="form-control" id="admission_date" name="admission_date" value="{{ $user->admission_date }}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="department">Setor: </label>
                    <select class="form-control" name="department_id" id="department" disabled>
                        <option></option>
                        @foreach($departments as $department)
                          <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endforeach
                        
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="branch">Filial</label>
                    {{-- <input type="text" class="form-control" id="branch" value="{{ $user->branch }}" readonly> --}}
                    <select class="form-control" name="branch_id" id="branch" disabled>
                        <option></option>
                        @foreach($branches as $branch)
                          <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                        @endforeach
                        
                      </select>
                  </div>
                  <div class="form-group">
                    <label for="role_id">Grupo de Permissões</label>
                     
                      <select class="form-control" name="role_id" id="role" disabled>
                        <option></option>
                        @foreach($roles as $role)
                          <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                        
                      </select>

                  </div>
                  {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
              </form>
              </div>
            </div>

            
          </div>
          <!-- /.col-md-12 -->
          <div class="col-md-4">
            <div class="card">
              <div class="card-body">
                <div class="row d-flex justify-content-center mb-3">
                  <h4>Funções</h4>
                </div>
                @if($is_user_profile)
                <div class="row">
                  <div class="col-md-6">
                      <a href="{{ route('updatePassword')}}" class="btn btn-primary btn-block">Alterar senha</a>
                  </div>
                  <div class="col-md-6">
                    <a href="{{ route('myPaychecks') }}" class="btn btn-warning btn-block">Seus Holerites</a>
                  </div>
                </div>
                @else

                <div class="row">
                  <div class="col-md-6">
                      <a href="{{ route('resetPassword', [$user->id]) }}" id="resetPassBtn" class="btn btn-success btn-block">Redefinir senha</a>
                  </div>
                  <div class="col-md-6">
                    <a href="{{ route('userMonths', [$user->id])}}" class="btn btn-warning btn-block">Holerites</a>
                  </div>
                </div>
                <div class="row mt-2">
                  <div class="col-md-12">
                    <a href="#" id="editProfileBtn" class="btn btn-info btn-block">Editar Funcionário</a>
                  </div>
                </div>
                @if($user->is_active)
                <div class="row mt-2">
                  <div class="col-md-12">
                    <a href="{{ route('deactivateUser', [$user->id]) }}" id="deactivateBtn" class="btn btn-danger btn-block">Desativar Funcionário</a>
                  </div>
                </div>
                @else
                <div class="row mt-2">
                  <div class="col-md-12">
                    <a href="{{ route('activateUser', [$user->id]) }}" id="deactivateBtn" class="btn btn-primary btn-block">Ativar Funcionário</a>
                  </div>
                </div>
                @endif
                @endif
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    {{-- Script para setar como selected a option correta --}}
    <script type="text/javascript">
      const role = document.getElementById('role');
      const branch = document.getElementById('branch');
      const department = document.getElementById('department');
      
      role.options.selectedIndex = {{ $user_role_id }}
      branch.options.selectedIndex = {{ $user->branch_id }}
      department.options.selectedIndex = {{ $user->department_id }}

      const resetPassBtn = document.getElementById('resetPassBtn');
      const editProfileBtn = document.getElementById('editProfileBtn');
      const deactivateBtn = document.getElementById('deactivateBtn');
      
      editProfileBtn.addEventListener('click', (e) => activateEditingMode());

      resetPassBtn.addEventListener('click', (e) => {
        const confirm_text = 'Ao redefinir a senha o usuário volta a ter a senha padrão "oeste\@tecnologia" até o seu próximo login. Você tem certeza que deseja redefinir a senha para esse usuário?'
        if (confirm(confirm_text)) {
          return true;
        } else {
          e.preventDefault();
        }

      })

      deactivateBtn.addEventListener('click', (e) => {
        const confirm_text = 'Ao desativar esse funcionário ele perderá acesso ao intranet e não será listado nas telas do sistema. Você só conseguirá encontrá-lo através da ferramenta de busca. Deseja prosseguir?'
        if (confirm(confirm_text)) {
          return true;
        } else {
          e.preventDefault();
        }
      })
      const activateEditingMode = () => {
        const form = document.getElementById('form');
        const inputs = Array.from(form.getElementsByTagName('input'));
        const selects = Array.from(form.getElementsByTagName('select'));
        inputs.map((input) => {
          input.readOnly = false;
        })
        selects.map((select) => {
          select.disabled = false;
        })
        const submit = document.createElement('input');
        submit.setAttribute('type', 'submit');
        submit.setAttribute('value', 'Salvar alterações');
        submit.classList.add('btn', 'btn-secondary',  'btn-lg', 'btn-block')
        form.appendChild(submit);

      }


    </script>
@endsection