@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center"><img src="https://yata-apix-61589086-5007-4240-9eec-744605e3805f.lss.locawebcorp.com.br/7d4eff6626864e6ab5be2ae448c898df.png" width="200"></div>

                <div class="card-body">
                    <div class="text-center mb-4 px-5 bg-alert">
                        <span class="small text-secondary"> Este é o sistema interno da Oeste Tecnologia, restrito apenas para funcionários. Informe como login o seu CPF e e a sua senha.</span>
                    </div>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="cpf" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <input id="cpf" type="number" class="form-control @error('register') is-invalid @enderror" name="cpf" value="{{ old('cpf') }}" required autofocus placeholder="CPF sem pontuação: ">

                                @error('register')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-right"></label>

                            <div class="col-md-6 mb-3">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Senha: ">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            @if ($errors->any())
                                    <div class="col-md-12">
                                        <div class="row d-flex justify-content-center">
                                                @foreach ($errors->all() as $error)
                                                    <li class="text-center  alert alert-danger text-center small" style="text-decoration: none">{{ $error }}</li>
                                                @endforeach
                                        </div>
                                    </div>
                                @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-dark">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link text-white bg-dark" href="{{ route('password.request') }}">
                                        {{ __('Esqueceu sua senha?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
