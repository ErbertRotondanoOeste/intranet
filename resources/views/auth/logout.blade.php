@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <span class="float-left inline lead mt-1">

                    </span>
                </div>

                <div class="card-body">
                        <div class="alert alert-success" role="alert">
                        </div>

                        Deslogou
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
