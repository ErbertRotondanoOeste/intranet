<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Paycheck;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $user = Auth::user();

        $paychecks         = Paycheck::where(['user_id' => $user->id])->get();
        $classname         = 'primary';
        $any_to_sign       = false;
        foreach ($paychecks as $paycheck) {
            if(!$paycheck->is_signed){
                $classname = 'warning';
                $any_to_sign = true;
            } 
        }

        $role = $user->roles()->first()->name;
        $params = [
            'user'          => $user,
            'classname'     => $classname,
            'any_to_sign'   => $any_to_sign,
            'role'          => $role
        ];
        return view('admin.homepanel', $params);
    }
    public function back(){
        return redirect()->back();
    }
    public function logout(){
        return view('auth.logout');
    }

    public function admin(){
        return view('permissions.admin');
    }

    public function user(){
        return view('permissions.user');
    }
    public function helpme(){
        $user = Auth::user();

        $role = $user->roles()->first()->name;

        $admin          = $role == 'admin';
        $diretoria      = $role == 'diretoria';
        $rh             = ($role == 'rh' || $role == 'admin' || $role == 'diretoria');
        $gerencia       = ($role == 'gerencia' || $role == 'admin' || $role == 'diretoria');
        $financeiro     = ($role == 'financeiro' || $role == 'admin' || $role == 'diretoria');

        $params = [
            'admin'          => $admin,
            'rh'             => $rh,
            'diretoria'      => $diretoria,
            'gerencia'       => $gerencia,
            'financeiro'     => $financeiro
        ];
        return view('user.helpme', $params);

    }

}
