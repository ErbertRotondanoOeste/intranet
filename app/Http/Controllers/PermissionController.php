<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Role;
use App\RoleUser;
use App\User;

class PermissionController extends Controller
{
    public function permissionDenied(){
        return view('permissions.permission_denied');
    }
    public function permissionGroups(){
    	
    	$roles = Role::all();

    	return view('permissions.permission_groups', ['groups' => $roles]);
    }
    public function create ( Request $request ) {
    	switch($request->method()){
    		case 'GET':
    			return view('permissions.create');
    		break;
    		case 'POST':
    			$role = new Role();
    			$role->name = Str::slug($request->post('name'), '-');

    			$role->save();
    			return redirect(route('permissionGroups'));
    		break;
    		default:
    		break;

    	}
    }
    public function view ($role_id) {
        $role_users = RoleUser::where(['role_id' => $role_id])->get();
        $role = Role::find($role_id);
        $users = [];
        foreach($role_users as $role_user){
            $user = User::find($role_user->user_id);
            array_push($users, $user);
        }
        $params = [
            'users' => $users, 
            'role' => $role
        ];
        return view("permissions.permission_users", $params);
    }
    public function addUser($role_id){
        $role_users = RoleUser::where(['role_id' => $role_id])->get();
        $role = Role::find($role_id);
        $users  = User::where(['is_active' => true])->get();

        $params = [
            'role' => $role,
            'users' => $users
        ];

        return view("permissions.add_user_to_group", $params);
    }
    public function insertUserInGroup(Request $request, $role_id, $user_id){
        
        $role_user = RoleUser::where(['user_id' => $user_id])->update(['role_id' => $role_id]);
        return redirect(route('permissionUsers', ['role_id' => $role_id]));
    }
}
