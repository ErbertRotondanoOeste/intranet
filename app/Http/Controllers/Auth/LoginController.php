<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('cpf', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            if(Auth::user()->is_first_login){
                return redirect(route('updatePassword'));
            } else {
                return redirect()->intended('/');
            }
        } else {
            $errors = [$this->username() => 'Verifique se está inserindo os dados de autenticação corretos'];
            return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);

        }
    }
}
