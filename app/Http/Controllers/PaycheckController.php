<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Month;
use App\Paycheck;
use App\User;
use App\File;
use App\Branch;
use Carbon\Carbon;
use Auth;

class PaycheckController extends Controller
{
    public function months() {
    	$months = Month::all()->sortBy('reference');

    	return view('paychecks.months', ['months' => $months]);
    }
    public function users($month_id) {

    	$paychecks = Paycheck::where(['month_id' => $month_id])->get();
    	$users_ids = [];
        $month = Month::find($month_id);
    	foreach($paychecks as $paycheck){
    		array_push($users_ids, $paycheck->user_id);
    	}
    	$users = User::find($users_ids);
        foreach($users as $user){
            $paychecks = Paycheck::where(['user_id' => $user->id, 'month_id' => $month->id])->get();
            foreach($paychecks as $paycheck){
                if(!$paycheck->is_signed){
                    $user->is_signed = false;
                }
            }
        }
    	$params = [
    		'users' 	=> $users,
    		'month' 	=> $month
    	];
    	return view('paychecks.users', $params);
    }
    public function paycheckdetail($paycheck_id, Request $request) {
    	$paycheck           = Paycheck::find($paycheck_id);
        $paycheck_owner     = User::find($paycheck->user_id);
        $user               = Auth::user();
        $role               = $user->roles()->first()->name;

        $is_privileged_user = ($role == 'rh' || $role == 'diretoria' || $role == 'admin');
        if( ($user->id == Auth::id() || $is_privileged_user) ){
            if($paycheck->is_signed){
                $paycheck->signed_at = date('d/m/Y - h:m:s', strtotime($paycheck->signed_at));
            }
        	$month 	            = Month::find($paycheck->month_id);
        	
            
            $file   = File::find($paycheck->file_id);
        	$params	= [
        		'paycheck' 	=> $paycheck,
        		'user'		=> $paycheck_owner,
        		'month'		=> $month,
                'file'      => $file,
                'is_privileged_user' => $is_privileged_user
        	];
        	return view('paychecks.detail', $params);
        }
        else {
            return redirect(route('nopermission'));
        }

    }
    public function userMonths($user_id){
        $user = User::find($user_id);
        $paychecks = Paycheck::where(['user_id' => $user_id])->get();
        $months_ids = [];
        foreach ($paychecks as $paycheck) {
            array_push($months_ids, $paycheck->month_id);
        }
        $months = Month::find($months_ids);
        foreach($months as $month){
            $paychecks = Paycheck::where(['user_id' => $user_id, 'month_id' => $month->id])->get();
            $month->is_signed = true;
            foreach($paychecks as $paycheck){
                if(!$paycheck->is_signed){
                    $month->is_signed = false;
                }
            }
        }
        $params = [
            'user'      => $user,
            'months'    => $months
        ];
        
        return view('user.paycheck_months', $params);
    }
    public function userPaychecks($user_id, $month_id){
        $user       = User::find($user_id);
        $paycheck   = Paycheck::where(['user_id' => $user_id, 'month_id' => $month_id])->first();
        $month      = Month::find($month_id);
        $file       = File::find($paycheck->file_id);
        $file_path  = Storage::path($file->name);
        $file_id    = $file->id;

        $params = [
            'user'      => $user,
            'month'     => $month,
            'paycheck'  => $paycheck,
            'file'      => $file
        ];

        return view('user.paycheck', $params);
    }
    public function signPaycheck($paycheck_id){
        $paycheck = Paycheck::find($paycheck_id);

        $paycheck->is_signed = true;
        $paycheck->signed_at = Carbon::now();
        $user_id = Auth::id();

        $paycheck->save();
        return redirect(route('userMonths', ['user_id' => $user_id]));

    }
    public function create(){
        $users  = User::where(['is_active' => true])->get();
        $months = Month::all();
        $params = [
            'users'     => $users,
            'months'    => $months
        ];
        return view('paychecks.create', $params);
    }
    public function store(Request $request){
        $request->validate([
            'file' => 'required|mimes:pdf|max:2048'
        ]);
        $month_id   = $request->post('month_id');
        $user_id    = $request->post('user_id');
        $paycheck   = new Paycheck();
        $file  = new File;

        if($request->file()) {
            $fileSlug = time().'_'.$request->file->getClientOriginalName();

            $file->name = time().'_'.$request->file->getClientOriginalName();
            $file->file_path = $fileSlug;
            $content = file_get_contents($request->file('file')->getRealPath());
            Storage::disk('local')->put($fileSlug, $content);

            $file->save();

            $paycheck->month_id = $month_id;
            $paycheck->user_id  = $user_id;
            $paycheck->file_id  = $file->id;
            $paycheck->save();
            
            return back()
            ->with('success','File has been uploaded.')
            ->with('file', $fileSlug);
        }
        
        
    }
    public function batchCreate(Request $request){
        
        switch ($request->method()) {
            case 'GET':
                $months = Month::all();
                return view('paychecks.batch', ['months' => $months]);
                break;
            case 'POST':
                $files = $request->file('files');
                $users = [];
                $month_id = $request->post('month_id');
                $errors = [];
                foreach ($files as $item) {
                    $file = new File();

                    $file->name         = $item->getClientOriginalName();
                    $file->file_path    = time().'_'.$item->getClientOriginalName();
                    $file->content      = file_get_contents($item->getRealPath());
                    $file               = $file->upload();
                    $file_name          = explode(".", $file->name)[0];
                    $user               = User::where(['name' => $file_name])->first();
                    if ($user) {    
                        $paycheck           = new Paycheck();
                        $paycheck->user_id  = $user->id;
                        $paycheck->month_id = $month_id;
                        $paycheck->file_id  = $file->id;

                        $paycheck->save();   
                    } else {   
                        $error_row = [
                            'message'   => 'Não foi possível encontrar esse usuário',
                            'user'      => $file_name
                        ];
                        array_push($errors, $error_row);
                    }
                }
                return view('paychecks.batch_results', ['errors' => $errors]);
                break;
            default:
                # code...
                break;
        }
        
    
    }
    public function paycheckHelper(){
        $branches = Branch::all();
        return view('paychecks.helper', ['branches' => $branches]);
    }
    public function downloadPaycheckHelper($branch_id){

        $users = User::where(['branch_id' => $branch_id, 'is_active' => true])->get()->sortBy('name');
        $branch = Branch::find($branch_id);
        $content = '';
        $counter = 1;
        foreach($users as $user){
            if(isset($user->roles()->first()->name)){
                $user_role = $user->roles()->first()->name;
            } else {
                // Roles não encontradas
            }
            

            if($user_role != 'diretoria'){
                $content = $content.'ren '.$counter.'_Holerites.pdf "'.$user->name.'.pdf" | ';
                $counter++;
            }
            else{
                // Um diretor passou aqui e ele não deve estar na listagem do arquivo
            }
            
        }
        $content = $content."echo 'deu certo'";
        $file = new File();
        $file->name = time().'_'.$branch->name.'HoleriteHelper.bat';
        $file->file_path = time().'_'.$branch->name.'HoleriteHelper.bat';

        Storage::disk('local')->put($file->name, $content);
        $file->save();

        return Storage::download($file->file_path);
    }
    public function createMonth(Request $request){
        switch($request->method()){
            case 'GET':
                $year = date('Y');
                return view('paychecks.createMonth', ['year' => $year]);       
            break;
            case 'POST':
                $month = new Month();
                $month_names = [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
                ];
                $reference = $request->post('month_number')."/".$request->post('year');
                $month->reference   = $reference;
                $month->number      = $request->post('month_number');
                $month->name        = $month_names[($request->post('month_number') - 1)];
                $month->save();
                return redirect(route('months'));
            break;
            default:
                return redirect(route('nopermission'));
            break;
        }
    }
    public function paycheckList($month_id, $user_id){
        $user       = User::find($user_id);
        $month      = Month::find($month_id);
        $paychecks  = Paycheck::where(['user_id' => $user_id, 'month_id' => $month_id])->get();
        
        foreach($paychecks as $paycheck){
            $paycheck->file = File::find($paycheck->file_id);
            $paycheck->title = $paycheck->file->name;
        }

        return view('paychecks.paychecks_list', ['paychecks' => $paychecks]);

    }
    public function disablePaycheck($paycheck_id){
        $paycheck = Paycheck::find($paycheck_id);

        $paycheck->is_active = false;
        $paycheck->save();
        return redirect(route('paycheckDetails', [$paycheck_id]));
    }
    public function enablePaycheck($paycheck_id){
        $paycheck = Paycheck::find($paycheck_id);

        $paycheck->is_active = true;
        $paycheck->save();
        return redirect(route('paycheckDetails', [$paycheck_id]));
    }
    public function myPaycheckMonths(){
        $user = Auth::user();
        $paychecks = Paycheck::where(['user_id' => $user->id])->get();
        $months_ids = [];
        foreach ($paychecks as $paycheck) {
            array_push($months_ids, $paycheck->month_id);
        }
        $months = Month::find($months_ids);
        foreach($months as $month){
            $paychecks = Paycheck::where(['user_id' => $user->id, 'month_id' => $month->id])->get();
            $month->is_signed = true;
            foreach($paychecks as $paycheck){
                if(!$paycheck->is_signed){
                    $month->is_signed = false;
                }
            }
        }
        $params = [
            'user'      => $user,
            'months'    => $months
        ];
        
        return view('user.my_paycheck_months', $params);
    }
    public function myPaychecks(){
        $user = Auth::user();
        $month      = Month::find($month_id);
        $paychecks  = Paycheck::where(['user_id' => $user->id, 'month_id' => $month_id])->get();
        
        foreach($paychecks as $paycheck){
            $paycheck->file = File::find($paycheck->file_id);
            $paycheck->title = $paycheck->file->name;
        }

        return view('paychecks.paychecks_list', ['paychecks' => $paychecks]);
    }
}
