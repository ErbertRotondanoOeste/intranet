<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Department;
use App\Branch;
use App\User;
use App\RoleUser;
use App\Role;

class UserController extends Controller
{
    public function profile($user_id){
    	$user = User::find($user_id);
        $user->admission_date = date('Y-m-d', strtotime($user->admission_date));
        // $user->admission_date = date('d/m/Y', strtotime($user->admission_date));
    	$role_user = RoleUser::where(['user_id' => $user->id])->get()->first();
        $authenticated_user_role = RoleUser::where(['user_id' => Auth::id()])->get()->first()->role_id;
        
        $is_user_allowed = (
            Auth::user()->roles()->first()->name == 'rh' || 
            Auth::user()->roles()->first()->name == 'admin' || 
            Auth::user()->roles()->first()->name == 'diretoria'
        );
        
        $is_user_profile = Auth::id() == $user_id;

        if($is_user_profile || $authenticated_user_role == 1 || $is_user_allowed){    
        	$user_role_id = Role::find($role_user)->first()->id;
        	$roles = Role::all();
            $branches = Branch::all();
            $departments = Department::all();
        	$department_id = $user->department_id;
        	$branch_id = $user->branch_id;
        	$user->department = Department::find($department_id)->name;
        	$user->branch = Branch::find($branch_id)->name;
            

        	$params = [
        		'user' 			    => $user, 
        		'roles' 		    => $roles,
                'branches'          => $branches,
        		'user_role_id'     	=> $user_role_id,
                'departments'       => $departments,
                'is_user_profile'   => $is_user_profile
        	];
        	return view('user.profile', $params);
        } else {
            return redirect(route('nopermission'));
        }
    }
    public function resetPassword($user_id){
        $user                   = User::find($user_id);
        $user->password         = Hash::make('oeste@tecnologia');
        $user->is_first_login   = true;

        $user->save();
        return redirect(route('getUsers'));
    }
    public function create(Request $request){
        switch($request->method()){
            case 'GET':
                $params = [
                    'departments'   => Department::all(),
                    'branches'      => Branch::all(),
                    'roles'         => Role::all()
                ];
                return view('user.create', $params);
            break;
            case 'POST':
                $validatedData = $request->validate([
                    'email' => ['required', 'unique:users'],
                    'register' => ['required', 'unique:users']
                ]);
                $user = new User();

                $user->name = $request->post('name');
                $user->email = $request->post('email');
                $user->admission_date = $request->post('admission_date');
                $user->department_id = $request->post('department');
                $user->branch_id = $request->post('branch');
                $user->register = $request->post('register');
                $user->cpf = $request->post('cpf');
                $user->password = Hash::make('oeste@tecnologia');
                $user->save();
                
                $role_user = new RoleUser();
                $role_user->role_id = $request->post('role');
                $role_user->user_id = $user->id;
                $role_user->save();
                return redirect(route('home'));

            break;
            default:
                return redirect(route('nopermission'));
            break;

        }

        
    }
    public function list() {
        $users = User::orderBy('name', 'asc')->where('is_active', true)->paginate(15);
        return view('user.list', ['users' => $users]);
    }
    public function updatePassword(Request $request){
        $user = Auth::user();
        switch($request->method()){
            case 'GET':
                return view('user.updatepassword', ['is_first_login' => $user->is_first_login]);
            break;
            case 'POST':
                $user = Auth::user();
                User::where(['id' => $user->id])->update([
                    'password'          => Hash::make($request->post('password')),
                    'is_first_login'    => false
                ]);
                return redirect(route('home'));
            break;
            default:
                return redirect(route('nopermission'));
            break;

        }
        
    }
    public function listBy($type){
        switch ($type) {
            case 'department':
                $data = Department::all();
                $data->typename = 'Setor';
                foreach ($data as $department) {
                    $users = User::where(['is_active' => true, 'department_id' => $department->id])->get();
                    $department->users = $users;
                }
                break;
            case 'branch':
                $data = Branch::all();
                $data->typename = 'Setor';
                foreach ($data as $branch) {
                    $users = User::where(['is_active' => true, 'branch_id' => $branch->id])->get();
                    $branch->users = $users;
                }
                break;
            default:

                break;
        }

        $params = [
                'data'   => $data,
        ];
        return view('user.list_by', $params);
    }
    public function search(){
        return view('user.search');
    }
    public function searchResults(Request $request){
        $name = $request->post('name');
        $result = User::where('name', 'like', '%'.$name.'%')->paginate(15);
        return view('user.search_results', ['users' => $result]);
    }
    public function userProfile($user_id){
        $user = User::find($user_id);
        return view('user.profile', ['user' => $user]);
    }
    public function updateUser($user_id, Request $request){
        $user = User::find($user_id);
        $user->name = $request->post('name');
        $user->email = $request->post('email');
        $user->register = $request->post('register');
        $user->admission_date = $request->post('admission_date');
        $user->department_id = $request->post('department_id');
        $user->branch_id = $request->post('branch_id');
        $user->cpf = $request->post('cpf');
        $user->save();
        return redirect(route('profile', ['user_id' => $user->id]));
    }
    public function deactivateUser($user_id){
        $user = User::find($user_id);

        $user->is_active = false;
        $user->save();
        return redirect(route('getUsers'));
    }
    public function activateUser($user_id){
        $user = User::find($user_id);

        $user->is_active = true;
        $user->save();
        return redirect(route('getUsers'));
    }
    public function disabledUsers(){
        $users = User::where(['is_active' => false])->paginate(15);
        return view('user.search_results', ['users' => $users]);
    }
}
