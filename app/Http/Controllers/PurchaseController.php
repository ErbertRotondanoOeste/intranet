<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Purchase;
Use App\PurchaseFile;
Use App\File;
Use App\Branch;
Use App\User;
Use App\Manager;
Use Auth;

class PurchaseController extends Controller
{
    public function create(Request $request){
    	switch ($request->method()) {
    		case 'GET':
    			return view('purchases.create');
    			break;
    		case 'POST':
                $purchase = new Purchase();
                $user = Auth::user();
                $purchase->title = $request->post('title');
                $purchase->description = $request->post('description');
                $purchase->value = $request->post('value');
                $purchase->user_id = $user->id;
                $purchase->branch_id = $user->branch_id;
                $purchase->save();
                return redirect(route('userPurchases'));
                break;
    		default:
    			# code...
    			break;
    	}
    }
    public function user_purchases(){
        $user = Auth::user();
        $purchases = Purchase::where(['user_id' => $user->id ])->get();
        foreach($purchases as $purchase){
            switch ($purchase->approval_level) {
            case 0:
                $purchase->approval_message = 'Negado';
                break;
            case 1:
                $purchase->approval_message = 'Aguardando aprovação do gerente responsável';
                break;
            case 2:
                $purchase->approval_message = 'Aguardando aprovação da diretoria';
                break;
            default:
                $purchase->approval_message = 'Aprovado';
                break;
            }
        }
        $params = [
            'purchases'     => $purchases,
            'user'          => $user
        ];
        return view('purchases.user_purchases', $params);
    }
    public function details($purchase_id){
        $purchase       = Purchase::find($purchase_id);
        $branch         = Branch::find($purchase->branch_id)->name;
        $user           = Auth::user();
        $purchase_owner = User::find($purchase->user_id);

        switch ($purchase->approval_level) {
            case 0:
                $purchase->approval_message = 'Negado';
                break;
            case 1:
                $purchase->approval_message = 'Aguardando aprovação do gerente responsável';
                break;
            case 2:
                $purchase->approval_message = 'Aguardando aprovação da diretoria';
                break;
            default:
                $purchase->approval_message = 'Aprovado';
                break;
        }
        $user->role = $user->roles->first()->name;
        $params = [
            'purchase'          => $purchase,
            'branch'            => $branch,
            'user'              => $user,
            'purchase_owner'    => $purchase_owner
        ];
        return view('purchases.details', $params);
    }
    public function managerPurchases(){
        $user               = Auth::user();
        $user_is_allowed    = false;
        $manager    = Manager::where(['user_id' => $user->id])->first();

        foreach($user->roles as $role){
            if($role->name == 'admin' || $role->name == 'gerencia' || $role->name == 'diretoria'){
                $user_is_allowed = true;
            }
        }
        if($user_is_allowed){
            if($manager){
                $purchases = Purchase::where(['branch_id' => $manager->branch_id])->orderBy('approval_level')->get();
                foreach ($purchases as $purchase) {
                    switch ($purchase->approval_level) {
                        case 0:
                            $purchase->approval_message = 'Negado';
                            break;
                        case 1:
                            $purchase->approval_message = 'Aguardando aprovação do gerente responsável';
                            break;
                        case 2:
                            $purchase->approval_message = 'Aguardando aprovação da diretoria';
                            break;
                        default:
                            $purchase->approval_message = 'Aprovado';
                            break;
                    }
                }
                $params = [
                    'purchases'     => $purchases,
                    'user'          => $user
                ];
                return view('purchases.managers', $params);
            } else {
                $purchases = Purchase::where(['approval_level' => 2])->get();
                foreach ($purchases as $purchase) {
                    $branch = Branch::find($purchase->branch_id);
                    $purchase->branch_name = $branch->name;
                    switch ($purchase->approval_level) {
                        case 0:
                            $purchase->approval_message = 'Negado';
                            break;
                        case 1:
                            $purchase->approval_message = 'Aguardando aprovação do gerente responsável';
                            break;
                        case 2:
                            $purchase->approval_message = 'Aguardando aprovação da diretoria';
                            break;
                        default:
                            $purchase->approval_message = 'Aprovado';
                            break;
                    }
                }
                $params = [
                    'purchases'     => $purchases,
                    'user'          => $user
                ];
                return view('purchases.directors', $params);
            }
        } else {
            return redirect(route('nopermission'));
        }
    }

    public function approveSecondPhase($purchase_id){
        $purchase = Purchase::find($purchase_id);
        $purchase->approval_level = 2;
        $purchase->save();
        return redirect(route('managerPurchases'));
    }
    public function approveThirdPhase($purchase_id){
        $purchase = Purchase::find($purchase_id);
        $purchase->approval_level = 3;
        $purchase->save();
        return redirect(route('managerPurchases'));
    }
    public function purchaseFiles($purchase_id){
        $purchase_files     = PurchaseFile::where(['purchase_id' => $purchase_id])->get();
        $purchase           = Purchase::find($purchase_id);
        $purchase_files_id  = [];
        foreach ($purchase_files as $purchase_file) {
            array_push($purchase_files_id, $purchase_file->id );
        }
        $files = File::find($purchase_files_id);
        $params = [
            'files'     => $files,
            'purchase'  => $purchase
        ];
        return view('purchases.files', $params);
    }
    public function createPurchaseFiles($purchase_id, Request $request){
        switch ($request->method()) {
            case 'GET':
                return view('purchases.create_files', ['purchase_id' => $purchase_id]);
                break;
            case 'POST':
                $files = $request->file('files');
                $month_id = $request->post('month_id');
                
                foreach ($files as $item) {
                    $file               = new File();
                    $file->name         = $item->getClientOriginalName();
                    $file->file_path    = time().'_'.$item->getClientOriginalName();
                    $file->content      = file_get_contents($item->getRealPath());
                    $file               = $file->upload();
                    $file_name          = explode(".", $file->name)[0];
                    $user               = User::where(['name' => $file_name])->first();
                    $purchase_file              = new PurchaseFile();
                    $purchase_file->purchase_id = $purchase_id;
                    $purchase_file->file_id     = $file->id;

                    $purchase_file->save();

                }
                return redirect(route('purchaseFiles', $purchase_id));
                break;
            default:
                # code...
                break;
        }
    }
    public function removeFile($purchase_id, $file_id){
        $file = File::find($file_id);
        $file->remove();
        return redirect(route('purchaseFiles', $purchase_id));
    }
    public function payPurchase($purchase_id){
        $purchase = Purchase::find($purchase_id);
        $purchase->is_paid = true;
        $purchase->save();
        return redirect(route('purchaseDetails', $purchase_id, Auth::id()));
    }
    public function allPurchases($field = 'created_at', $order = 'desc', Request $request){
        
        $is_ordered = false;

        if($request->get('field') && $request->get('order')){
            $field      = $request->get('field');
            $order      = $request->get('order');
            $is_ordered = true;
        }
        $purchases = Purchase::orderBy($field, $order)->paginate(15);

        
        // dd($purchases);
        foreach ($purchases as $purchase) {
            switch ($purchase->approval_level) {
                case 1:
                    $purchase->approval_message = 'Aguardando aprovação do gerente responsável';
                    break;
                case 2:
                    $purchase->approval_message = 'Aguardando aprovação da diretoria';
                    break;
                case 0:
                    $purchase->approval_message = 'Negado';
                    break;
                default:
                    $purchase->approval_message = 'Aprovado';
                    break;
            }
        }
        $params = [
            'purchases'     => $purchases,
            'user'          => Auth::user(),
            'is_ordered'    => $is_ordered
        ];
        return view('purchases.list_all', $params);

    }
    public function purchasesToPayList(){
        $purchases = Purchase::where(['is_paid' => false, 'approval_level' => 3])->paginate(10);
        $user = Auth::user();
        $params = [
            'purchases'     => $purchases,
            'user'          => $user
        ];
        return view('purchases.list', $params);
    }
    public function denySecondPhase($purchase_id){
        $purchase = Purchase::find($purchase_id);
        $purchase->approval_level = 0;
        $purchase->save();
        return redirect(route('purchaseDetails', $purchase_id, Auth::id()));
    }
    public function denyThirdPhase($purchase_id){
        $purchase = Purchase::find($purchase_id);
        $purchase->approval_level = 0;
        $purchase->save();
        return redirect(route('purchaseDetails', $purchase_id, Auth::id()));
    }
}
