<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $fillable = [
        'name',
        'file_path'
    ];
    public function upload(){
        
        $file = $this;
        Storage::disk('local')->put($file->file_path, $file->content);
        unset($file->content);
        $file->save();
        
        return $file;
    }
    public function remove(){
        $file = $this;
        Storage::disk('local')->delete($file->file_path);
        $file->delete();
        return $file;
    }
}
