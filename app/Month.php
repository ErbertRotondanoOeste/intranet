<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    public function paycheck()
    {
    	return $this->belongsToMany('App\Paycheck');
    }
}
