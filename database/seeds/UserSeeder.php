<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = 
[
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('')),
        'register' =>  1,
        'name' =>  'PAULO FRAGA',
        'password' =>  Hash::make('oeste@diretoria'),
        'cpf' =>  '01748005502','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('')),
        'register' =>  2,
        'name' =>  'JACKSON MARTINS',
        'password' =>  Hash::make('oeste@diretoria'),
        'cpf' =>  '00825911567','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('11/16/2020')),
        'register' =>  3,
        'name' =>  'ALAN NEVES DE SOUZA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07886083555','department_id' => 19 
    ],
    [
        'branch_id' => 3,
        'admission_date' =>  date('Y-m-d', strtotime('10/14/2020')),
        'register' =>  4,
        'name' =>  'ANDREZZA LUIZA GAMA CAMPOS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07676803594','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('8/16/2019')),
        'register' =>  5,
        'name' =>  'ANGYELE PATRICIA LOURENCO LOUZADA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07067340683','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('6/10/2020')),
        'register' =>  6,
        'name' =>  'BARBARA EMILLY FERNANDES NEVES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '12804683605','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('11/23/2020')),
        'register' =>  7,
        'name' =>  'BRUNA FREITAS COUTINHO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '11288893663','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('8/10/2020')),
        'register' =>  8,
        'name' =>  'BRUNA NUNES NASCIMENTO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06640767503','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('3/2/2020')),
        'register' =>  9,
        'name' =>  'CARLA FAINE RAMOS DE CARVALHO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07353824506','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('8/24/2020')),
        'register' =>  10,
        'name' =>  'CARLOS ROBERTO BISPO DE JESUS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '91213614520','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('4/20/2020')),
        'register' =>  11,
        'name' =>  'CAROLINE DE SOUZA CONSTANTINO DA SILVA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '86128961509','department_id' => 19 
    ],
    [
        'branch_id' => 3,
        'admission_date' =>  date('Y-m-d', strtotime('2/22/2021')),
        'register' =>  12,
        'name' =>  'CATARINE DOS SANTOS E SANTOS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '08702700522','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('2/18/2021')),
        'register' =>  13,
        'name' =>  'CRISTIANE MATOS DE SOUSA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '95344977549','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('7/28/2020')),
        'register' =>  14,
        'name' =>  'CRISTIANO ROCHA COPELLO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '78395496591','department_id' => 19 
    ],
    [
        'branch_id' => 3,
        'admission_date' =>  date('Y-m-d', strtotime('12/7/2020')),
        'register' =>  15,
        'name' =>  'DANIELLE DAVILLE OLIVEIRA LEITE',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '03313186521','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('8/31/2020')),
        'register' =>  16,
        'name' =>  'DEANE FRANCISCO DE SOUZA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04437073592','department_id' => 19 
    ],
    [
        'branch_id' => 3,
        'admission_date' =>  date('Y-m-d', strtotime('2/11/2019')),
        'register' =>  17,
        'name' =>  'DYELLE DOS SANTOS TEIXEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '','department_id' => 19 ]
        ,

    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('12/14/2020')),
        'register' =>  18,
        'name' =>  'EDILENE MEIRA TEIXEIRA DE ALMEIDA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '01925252558','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('4/20/2020')),
        'register' =>  19,
        'name' =>  'EDSONIA FIQUEIREDO DE JESUS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05513747561','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('1/5/2020')),
        'register' =>  20,
        'name' =>  'ELAINE KETYS SANTANA NASCIMENTO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '03251026518','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('6/22/2020')),
        'register' =>  21,
        'name' =>  'ELISLAINE BOA SORTE GUIMARAES OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05795660566','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('4/16/2015')),
        'register' =>  22,
        'name' =>  'ELVIS OLIVEIRA MEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06338685583','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('11/16/2020')),
        'register' =>  23,
        'name' =>  'ERBERT DE ALMEIDA ROTONDANO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06629235533','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('2/23/2021')),
        'register' =>  24,
        'name' =>  'ERICK MEIRA BORGES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '13784256694','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('8/17/2020')),
        'register' =>  25,
        'name' =>  'ERYDAN RIBEIRO COSTA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04095675586','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('3/1/2018')),
        'register' =>  26,
        'name' =>  'EVA ALVES PORTO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '72693169100','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('9/1/2017')),
        'register' =>  27,
        'name' =>  'FERNANDO CESAR LOPES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '01222170558','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('4/8/2019')),
        'register' =>  28,
        'name' =>  'FRANCILANE SANTOS MOURA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06461517545','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('8/14/2019')),
        'register' =>  29,
        'name' =>  'GILVANIA DIAS DURAES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '67750222634','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('11/7/2017')),
        'register' =>  30,
        'name' =>  'GUILHERME DOS SANTOS LIMA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05945048569','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('3/1/2019')),
        'register' =>  31,
        'name' =>  'GUILHERME HENRIQUE PIMENTEL OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06638004557','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('11/11/2020')),
        'register' =>  32,
        'name' =>  'GUSTAVO MEIRA MENDES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '08139175579','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('12/3/2020')),
        'register' =>  33,
        'name' =>  'GUSTAVO REIS LEAO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '03962995552','department_id' => 19 
    ],
    [
        'branch_id' => 3,
        'admission_date' =>  date('Y-m-d', strtotime('4/20/2020')),
        'register' =>  34,
        'name' =>  'HENRIQUE LOPES DE SOUZA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07307949547','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('8/17/2020')),
        'register' =>  35,
        'name' =>  'IURY ANDRADE LIMA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '14029801676','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('1/2/2019')),
        'register' =>  36,
        'name' =>  'IVAN DE OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '90303598549','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('12/23/2020')),
        'register' =>  37,
        'name' =>  'JADSON GABRIEL COSTA COELHO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07216891597','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('2/22/2021')),
        'register' =>  38,
        'name' =>  'JAIRO XAVIER DE ALMEIDA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04289073506','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('2/4/2020')),
        'register' =>  39,
        'name' =>  'JHONNY DOURADO DE LIMA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04792838185','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('5/2/2019')),
        'register' =>  40,
        'name' =>  'JOSEVAN DOS SANTOS DA SILVA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '08721545561','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('11/1/2014')),
        'register' =>  41,
        'name' =>  'JULIO CESAR PEREIRA RODRIGUES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '02207361543','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('7/11/2019')),
        'register' =>  42,
        'name' =>  'JUVANILSON CARDOSO DA SILVA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '49596845500','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('3/8/2021')),
        'register' =>  43,
        'name' =>  'KARINE BORBOREMA ARAUJO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '03475804590','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('5/18/2018')),
        'register' =>  44,
        'name' =>  'KEILA DE CASSIA ARNIZAUT COSTA MOURA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '01716470501','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('2/9/2021')),
        'register' =>  45,
        'name' =>  'LARISSA NOBRE COUTINHO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07446360607','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('8/28/2020')),
        'register' =>  46,
        'name' =>  'LIDIANE DOS SANTOS SILVA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05135485481','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('11/16/2020')),
        'register' =>  47,
        'name' =>  'LILIAN NASCIMENTO DA CONCEIÇÃO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '01487673540','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('8/14/2019')),
        'register' =>  48,
        'name' =>  'LUANA LIMA ALCANTARA PONTES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '08864311661','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('11/22/2019')),
        'register' =>  49,
        'name' =>  'LUANE BARBARA DE OLIVEIRA COSTA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06046523567','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('8/14/2019')),
        'register' =>  50,
        'name' =>  'LUIZ RASIELSON PEREIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '96565535604','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('10/1/2019')),
        'register' =>  51,
        'name' =>  'MARINA MAGALHÃES PEREIRA CASTRO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06388672582','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('3/1/2018')),
        'register' =>  52,
        'name' =>  'MAURICIO GONÇALVES DE OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05845022576','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('1/15/2021')),
        'register' =>  53,
        'name' =>  'MILENA CARLA DE OLIVEIRA SOUZA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05886413596','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('8/14/2019')),
        'register' =>  54,
        'name' =>  'NAIARA AGUIAR OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '10484061640','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('5/15/2015')),
        'register' =>  55,
        'name' =>  'NUBIA XAVIER DO NASCIMENTO',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '03415704513','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('2/4/2020')),
        'register' =>  56,
        'name' =>  'PRISCILA SANTANA GUIMARAES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '06131613605','department_id' => 19 
    ],
    [
        'branch_id' => 5,
        'admission_date' =>  date('Y-m-d', strtotime('8/10/2020')),
        'register' =>  57,
        'name' =>  'RAUL ALVES CRUZ',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '81518730515','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('10/2/2020')),
        'register' =>  58,
        'name' =>  'REBERT MORENO MARQUES',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '08418590513','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('3/2/2020')),
        'register' =>  59,
        'name' =>  'RHUAN SEBASTIAN PEREIRA RAMOS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04392805166','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('8/3/2020')),
        'register' =>  60,
        'name' =>  'SOLANGE PEREIRA DOS SANTOS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '33025733837','department_id' => 19 
    ],
    [
        'branch_id' => 1,
        'admission_date' =>  date('Y-m-d', strtotime('10/1/2020')),
        'register' =>  61,
        'name' =>  'SOLANGE PORTO DE OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04517171558','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('3/1/2018')),
        'register' =>  62,
        'name' =>  'TARCISIO ARAUJO SOARES REIS',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '04928089537','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('3/8/2018')),
        'register' =>  63,
        'name' =>  'THAIS OLIVEIRA COTRIM',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '05252993513','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('7/30/2019')),
        'register' =>  64,
        'name' =>  'THIAGO VINICIUS VALADARES MARQUEZ',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07315718605','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('10/24/2019')),
        'register' =>  65,
        'name' =>  'ULISSES DE SOUZA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '13335347609','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('9/28/2020')),
        'register' =>  66,
        'name' =>  'VALÉRIA ANTUNES PARADA FROTA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '24688857837','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('5/4/2020')),
        'register' =>  67,
        'name' =>  'VALÉRIA DE SOUSA E SILVA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '25777709877','department_id' => 19 
    ],
    [
        'branch_id' => 3,
        'admission_date' =>  date('Y-m-d', strtotime('1/25/2021')),
        'register' =>  68,
        'name' =>  'VANDERSON DE SOUZA PESSOA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '07426980502','department_id' => 19 
    ],
    [
        'branch_id' => 2,
        'admission_date' =>  date('Y-m-d', strtotime('1/19/2019')),
        'register' =>  69,
        'name' =>  'VIVIANE FERNANDES RALISSE',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '32985922852','department_id' => 19 
    ],
    [
        'branch_id' => 4,
        'admission_date' =>  date('Y-m-d', strtotime('3/1/2021')),
        'register' =>  70,
        'name' =>  'LUCAS CAUA GONCALVES DE OLIVEIRA',
        'password' =>  Hash::make('oeste@tecnologia'),
        'cpf' =>  '17682653647','department_id' => 19 
    ]
];
         DB::table('users')->insert($data);
    }
}
