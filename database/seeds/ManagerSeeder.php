<?php

use Illuminate\Database\Seeder;

class ManagerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'user_id' 	=> 41,
        		'branch_id'	=> 1
        	],
        	[
        		'user_id' 	=> 62,
        		'branch_id'	=> 2
        	],
        	[
        		'user_id' 	=> 15,
        		'branch_id'	=> 3
        	],
        	[
        		'user_id' 	=> 64,
        		'branch_id'	=> 4
        	],
        	[
        		'user_id' 	=> 14,
        		'branch_id'	=> 5
        	]
        ];
        DB::table('managers')->insert($data);

    }
}
