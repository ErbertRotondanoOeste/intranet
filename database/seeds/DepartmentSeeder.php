<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		['name' => 'COMERCIAL'],
            ['name' => 'FINANCEIRO'],
            ['name' => 'COMERCIAL/VENDAS'],
            ['name' => 'SERVIÇOS GERAIS'],
            ['name' => 'GERENTE FILIAL'],
            ['name' => 'SERVIÇOS EXTERNOS'],
            ['name' => 'ESTOQUE'],
            ['name' => 'CAIXA'],
            ['name' => 'CONTAS A PAGAR'],
            ['name' => 'GERENTE COMERCIAL'],
            ['name' => 'CONTAS A RECEBER'],
            ['name' => 'MARKETING'],
            ['name' => 'CONTÁBIL'],
            ['name' => 'COMPRAS'],
            ['name' => 'FISCAL'],
            ['name' => 'CONTÁBIL/FISCAL'],
            ['name' => 'COMERCIAL / ADM'],
            ['name' => 'RH'],
            ['name' => 'TI'],
            ['name' => 'COMERCIAL/ADM'],
            ['name' => 'COMERICAL/VENDAS'],
    	];
         DB::table('departments')->insert($data);
    }
}
