<?php

use Illuminate\Database\Seeder;
Use \Carbon\Carbon;

class PaycheckSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		[
    			'month_id'   => 1,
    			'user_id'    => '1',
                'is_signed'  => true,
                'signed_at'  => Carbon::now(),
                'file_id'    => 1
    		],
    		[
    			'month_id'   => 2,
    			'user_id'    => '1',
                'is_signed'  => true,
                'signed_at'  => Carbon::now(),
                'file_id'    => 1
    		],
    		[
    			'month_id'   => 3,
    			'user_id'    => '1',
                'is_signed'  => false,
                'signed_at'  => null,
                'file_id'    => 1
    		],
    		[
    			'month_id'   => 4,
    			'user_id'    => '1',
                'is_signed'  => false,
                'signed_at'  => null,
                'file_id'    => 1
    		],
            [
                'month_id'   => 1,
                'user_id'    => '2',
                'is_signed'  => true,
                'signed_at'  => Carbon::now(),
                'file_id'    => 1
            ],
            [
                'month_id'   => 2,
                'user_id'    => '2',
                'is_signed'  => true,
                'signed_at'  => null,
                'file_id'    => 1
            ],
            [
                'month_id'   => 3,
                'user_id'    => '2',
                'is_signed'  => false,
                'signed_at'  => null,
                'file_id'    => 1
            ],
            [
                'month_id'   => 4,
                'user_id'    => '2',
                'is_signed'  => false,
                'signed_at'  => null,
                'file_id'    => 1
            ],
            [
                'month_id'   => 1,
                'user_id'    => '4',
                'is_signed'  => true,
                'signed_at'  => null,
                'file_id'    => 1
            ],
            [
                'month_id'   => 2,
                'user_id'    => '4',
                'is_signed'  => true,
                'signed_at'  => null,
                'file_id'    => 1
            ],
            [
                'month_id'   => 3,
                'user_id'    => '4',
                'is_signed'  => false,
                'signed_at'  => null,
                'file_id'    => 1
            ],
            [
                'month_id'   => 4,
                'user_id'    => '4',
                'is_signed'  => false,
                'signed_at'  => null,
                'file_id'    => 1
            ],
    	];
         DB::table('paychecks')->insert($data);
    }
}
