<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$branches = [
    		['name' => 'Barreiras'],
    		['name' => 'Guanambi'],
    		['name' => 'Luiz Eduardo Magalhães'],
    		['name' => 'Montes Claros'],
    		['name' => 'Camaçari']
    	];
         DB::table('branches')->insert($branches);
    }
}
