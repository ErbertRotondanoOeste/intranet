<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		['name' => 'admin'],
            ['name' => 'rh'],
            ['name' => 'diretoria'],
            ['name' => 'gerencia'],
            ['name' => 'financeiro'],
            ['name' => 'geral']
    	];
         DB::table('roles')->insert($data);
    }
}
