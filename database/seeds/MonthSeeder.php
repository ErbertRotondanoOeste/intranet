<?php

use Illuminate\Database\Seeder;

class MonthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
    		[
    			'name' 		=> 'Dezembro',
    			'number'	=> 12,
    			'reference'	=> '12/2020'
    		],
    		[
    			'name' 		=> 'Janeiro',
    			'number'	=> 1,
    			'reference'	=> '01/2021'
    		],
    		[
    			'name' 		=> 'Fevereiro',
    			'number'	=> 2,
    			'reference'	=> '02/2021'
    		],
    		[
    			'name' 		=> 'Março',
    			'number'	=> 3,
    			'reference'	=> '03/2021'
    		],
            [
                'name'      => 'Abril',
                'number'    => 4,
                'reference' => '04/2021'
            ],
            [
                'name'      => 'Maio',
                'number'    => 5,
                'reference' => '05/2021'
            ],
            [
                'name'      => 'Junho',
                'number'    => 6,
                'reference' => '06/2021'
            ],

    	];
         DB::table('months')->insert($data);
    }
}
