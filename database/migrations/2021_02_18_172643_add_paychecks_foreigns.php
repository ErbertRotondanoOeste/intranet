<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaychecksForeigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paychecks', function($table) {
            $table->foreign('month_id')->references('id')->on('months')->onCascade('delete');
            $table->foreign('user_id')->references('id')->on('users')->onCascade('delete'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paychecks', function($table) {
            $table->dropForeign(['month_id', 'user_id']);
        });
        Schema::disableForeignKeyConstraints();
    }
}
