<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->integer('approval_level')->default(1);
            $table->decimal('value');
            $table->boolean('is_paid')->default(false);

            $table->foreignId('user_id')->unsigned();
            $table->foreignId('branch_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('purchases', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onCascade('delete');
            $table->foreign('branch_id')->references('id')->on('branches')->onCascade('delete'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
