<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->unsigned();
            $table->foreignId('branch_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('managers', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onCascade('delete');
            $table->foreign('branch_id')->references('id')->on('branches')->onCascade('delete'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('managers', function($table) {
            $table->dropForeign(['user_id', 'branch_id']);
        });
        Schema::dropIfExists('managers');
    }
}
