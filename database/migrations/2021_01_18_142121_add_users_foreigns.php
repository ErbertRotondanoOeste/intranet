<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsersForeigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function($table) {
            $table->foreign('department_id')->references('id')->on('departments')->onCascade('delete');
            $table->foreign('branch_id')->references('id')->on('branches')->onCascade('delete'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('users', function($table) {
            $table->dropForeign(['department_id', 'branch_id']);
        });
    }
}
