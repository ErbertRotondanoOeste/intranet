<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->integer('register');
            $table->string('name');
            $table->boolean('is_first_login')->default(true);
            $table->string('email')->unique()->nullable();
            $table->string('cpf')->unique();
            $table->date('admission_date')->nullable();
            $table->date('birth_date')->nullable();
            $table->bigInteger('department_id')->unsigned();
            $table->bigInteger('branch_id')->unsigned();
            $table->string('confirmation_email')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
